Pod::Spec.new do |s|
  s.name          = 'iCashPayFramework'
  s.version       = '1.0.0'
  s.summary       = 'iCashPayFramework for Pod Installation'
  s.homepage      = 'http://maxkalik.com'
  s.license       = { :type => 'SELF' }
  s.author        = { 'iCashPayFramework' => 'nobodyyu@gmail.com' }
  s.source        = { :http => 'https://gitlab.com/nobodyyu/icp/-/raw/main/iCashPayFramework.xcframework.zip' }
  s.swift_version = '5.0'
  s.ios.deployment_target = '11.0'

  s.dependency 'PromiseKit', '~> 6.8.3'
  s.dependency 'Alamofire', '~> 4.8.2'
  s.dependency 'Socket.IO-Client-Swift', '~> 15.1.0'
  s.dependency 'SVProgressHUD', '~> 2.2.5'
  s.dependency 'SnapKit', '~> 4.2.0'
  s.dependency 'IQKeyboardManagerSwift' ,'<= 6.2.0'
  s.dependency 'Kingfisher', '<= 5.7.1'
  s.dependency 'NotificationBannerSwift', '2.0.1'
  s.dependency 'SwifterSwift', '< 5.0.0'
  s.dependency 'ZSWTappableLabel', '~> 3.3.2'
  s.dependency 'KeychainSwift', '~> 16.0'
  s.dependency 'RealmSwift', '~> 3.18.0'
  s.dependency 'Realm', '~> 3.18.0'
  s.dependency 'SwiftyJSON', '~> 4.2.0'
  s.dependency 'SwiftyXMLParser', '~> https://github.com/yahoojapan/SwiftyXMLParser.git'
  s.dependency 'SwiftyRSA', '~> https://github.com/jannemecek/SwiftyRSA.git'
  s.dependency 'CryptoSwift', '<= 0.15.0'
  s.dependency 'FSPagerView', '0.8.2'


  s.vendored_frameworks = 'iCashPayFramework.xcframework'
end